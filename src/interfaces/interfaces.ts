export interface MarkersData {
  id: number;
  color: string;
  title: string;
  latitude: number;
  longitude: number;
  content: string;
}

export interface InitialMapState {
  latitude: number;
  longitude: number;
  latitudeDelta: number;
  longitudeDelta: number;
}
