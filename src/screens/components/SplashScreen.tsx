import React from 'react';
import {Image, StyleSheet} from 'react-native';
import {WINDOW_HEIGHT, WINDOW_WIDTH} from '../../consts/consts';

const SplashScreen = () => {
  return (
    <Image
      source={require('../../../assets/images/SplashImage.png')}
      style={styles.logo}
      resizeMode={'center'}
    />
  );
};

const styles = StyleSheet.create({
  logo: {
    backgroundColor: '#fff',
    width: WINDOW_WIDTH,
    height: WINDOW_HEIGHT,
  },
});

export default SplashScreen;
