import {action, makeAutoObservable, runInAction} from 'mobx';
import {onGetMarkersData} from '../api/MapApi';
import {InitialMapState, MarkersData} from '../interfaces/interfaces';

class MapStore {
  constructor() {
    makeAutoObservable(this);
  }

  markersList: MarkersData[] = [];
  isLoading = true;
  isSelectedMarker = null;

  @action
  async onGetMapData() {
    await this.loadData();
  }

  @action
  loadData = async () => {
    this.isLoading = true;
    await onGetMarkersData()
      .then(response => {
        runInAction(() => {
          if (response !== null) {
            this.markersList = [];
            this.markersList.push(...response);
            this.isLoading = false;
          } else {
            this.markersList = [];
            this.isLoading = false;
          }
        });
      })
      .catch(err => {
        this.isLoading = false;
        console.log(err);
      });
  };

  onSelectMarker = (marker: MarkersData, mapRef) => {
    const Coordinates = {
      latitude: marker.latitude,
      longitude: marker.longitude,
      latitudeDelta: 0.111,
      longitudeDelta: 0.111,
    };
    this.isSelectedMarker = marker;
    mapRef.current.animateToRegion(Coordinates);
  };

  onSelectMarkerById = (id: string, mapRef) => {
    if (this.markersList.length !== 0) {
      const selectedMarker = this.markersList.find(
        marker => marker.id.toString() === id,
      );

      if (selectedMarker) {
        console.log('success found marker');
        const Coordinates = {
          latitude: selectedMarker?.latitude,
          longitude: selectedMarker?.longitude,
          latitudeDelta: 0.111,
          longitudeDelta: 0.111,
        };
        this.isSelectedMarker = selectedMarker;
        setTimeout(() => mapRef.current.animateToRegion(Coordinates), 1000);
      } else {
        this.isSelectedMarker = selectedMarker;
        console.log('fail found marker');
      }
    }
  };

  onSetInitialMapState = (initialMapState: InitialMapState, mapRef) => {
    mapRef.current.animateToRegion(initialMapState);
  };
}

const mapStore = new MapStore();
export default mapStore;
