export function onGetMarkersData(): Promise<any> {
  const url =
    'https://raw.githubusercontent.com/fullstackPrincess/test/main/data.json';
  const options = {method: 'GET', headers: {accept: 'application/json'}};

  return fetch(url, options)
    .then(response => response.json())
    .then(responseJson => {
      return responseJson;
    })
    .catch(err => {
      console.error(err);
      throw err;
    });
}
