import {GestureDetector} from 'react-native-gesture-handler';
import Animated from 'react-native-reanimated';
import {StyleSheet, TouchableOpacity} from 'react-native';
import {size22, size32, WINDOW_WIDTH} from '../../consts/consts';
import BottomSheetContent from './BottomSheetContent';
import React from 'react';
import IconAntDesign from 'react-native-vector-icons/AntDesign';

const BottomSheet = ({
  pan,
  translateY,
  height,
  opacity,
  isIconTouchable,
  toggleSheet,
  scrollViewRef,
  isScrollingEnabled,
  isSelectedMarker,
}) => {
  return (
    <GestureDetector style={{backgroundColor: 'red'}} gesture={pan}>
      <Animated.View style={[styles.box, translateY, {height: height}]}>
        <Animated.View
          style={[
            {
              justifyContent: 'flex-end',
              alignItems: 'flex-end',
              paddingBottom: 6,
            },
            {opacity: opacity},
          ]}>
          <TouchableOpacity
            disabled={!isIconTouchable}
            onPress={() => toggleSheet(false)}
            style={{
              marginTop: 10,
              borderRadius: 100,
              backgroundColor: '#c7c8c8',
            }}>
            <IconAntDesign
              style={{padding: 6}}
              name={'close'}
              color={'#000'}
              size={size22}
            />
          </TouchableOpacity>
        </Animated.View>
        <BottomSheetContent
          scrollViewRef={scrollViewRef}
          isScrollingEnabled={isScrollingEnabled}
          isSelectedMarker={isSelectedMarker}
        />
      </Animated.View>
    </GestureDetector>
  );
};

const styles = StyleSheet.create({
  box: {
    paddingHorizontal: 24,
    borderTopLeftRadius: 24,
    borderTopRightRadius: 24,

    width: WINDOW_WIDTH,
    position: 'absolute',
    bottom: 0,
    zIndex: 1,
    backgroundColor: '#fff',
    shadowColor: '#000000',
    shadowOffset: {
      width: 0,
      height: 8,
    },
    shadowOpacity: 0.1,
    shadowRadius: 24,
    elevation: 10,
  },
});

export default BottomSheet;
