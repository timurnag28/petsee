import React, {useEffect, useRef, useState} from 'react';
import {Linking, View} from 'react-native';

import store from './store';
import {observer} from 'mobx-react';

import {Gesture} from 'react-native-gesture-handler';
import Animated, {
  useSharedValue,
  withTiming,
  useAnimatedGestureHandler,
  SlideInDown,
  SlideOutDown,
  useAnimatedStyle,
  withSpring,
  runOnJS,
} from 'react-native-reanimated';
import {initialMapState, WINDOW_HEIGHT} from './consts/consts';
import Map from './screens/components/Map';
import SplashScreen from './screens/components/SplashScreen';
import BottomSheet from './screens/components/BottomSheet';

const MainView = observer(() => {
  const [isScrollingEnabled, setScrolling] = useState(false);
  const [isIconTouchable, setIconTouchable] = useState(false);
  const scrollViewRef = useRef(null);
  const mapRef = useRef(null);
  const offset = useSharedValue(0);
  const height = useSharedValue(0);
  const opacity = useSharedValue(0);

  useEffect(() => {
    void getUrl();
  }, []);

  const getUrl = async () => {
    await store.mapStore.onGetMapData();
    const inititialUrl = await Linking.getInitialURL();
    if (inititialUrl === null) {
      return;
    }
    if (inititialUrl.includes('id')) {
      const [, id] = inititialUrl.split('id');
      try {
        await store.mapStore.onSelectMarkerById(id, mapRef);
        if (store.mapStore.isSelectedMarker) {
          toggleSheet(true);
        }
      } catch (error) {
        console.error(error);
      }
    }
  };

  const toggleSheet = (value: boolean) => {
    if (value) {
      offset.value = withSpring(0);
      height.value = withTiming(WINDOW_HEIGHT / 2);
    } else {
      setScrolling(false);
      height.value = withTiming(0);
      opacity.value = withTiming(0);
      setIconTouchable(false);
      scrollViewRef.current?.scrollTo({x: 0, y: 0, animated: false});
    }
    if (!value) {
      store.mapStore.onSetInitialMapState(initialMapState, mapRef);
    }
  };

  const fullSizeSheet = () => {
    height.value = withTiming(WINDOW_HEIGHT * 0.9);
    setScrolling(true);
    opacity.value = withTiming(1);
    setIconTouchable(true);
  };

  const handleDeepLink = async ({url}) => {
    toggleSheet(false);
    if (url === null) {
      return;
    }
    if (url.includes('id')) {
      const [, id] = url.split('id');
      try {
        await store.mapStore.onSelectMarkerById(id, mapRef);
        if (store.mapStore.isSelectedMarker) {
          toggleSheet(true);
        }
      } catch (error) {
        console.error(error);
      }
    }
  };

  const translateY = useAnimatedStyle(() => ({
    transform: [{translateY: offset.value}],
  }));

  const pan = Gesture.Pan()
    .onChange(event => {
      const offsetDelta = event.changeY + offset.value;
      const clamp = Math.max(-50, offsetDelta);
      offset.value = offsetDelta > 0 ? offsetDelta : withSpring(clamp);
    })
    .onFinalize(() => {
      if (offset.value < -1) {
        runOnJS(fullSizeSheet)();
      }
      if (offset.value < WINDOW_HEIGHT / 6) {
        offset.value = withSpring(0);
      } else {
        runOnJS(toggleSheet)(false);
      }
    });

  const setupDeepLinkListener = () => {
    Linking.addEventListener('url', handleDeepLink);
  };

  useEffect(() => {
    setupDeepLinkListener();
    return () => {
      Linking.removeEventListener('url', handleDeepLink);
    };
  }, []);
  const {isSelectedMarker, isLoading} = store.mapStore;
  if (isLoading) {
    return <SplashScreen />;
  } else {
    return (
      <View>
        <Map
          toggleSheet={toggleSheet}
          mapRef={mapRef}
          mapStore={store.mapStore}
        />
        <BottomSheet
          pan={pan}
          translateY={translateY}
          height={height}
          opacity={opacity}
          isIconTouchable={isIconTouchable}
          toggleSheet={toggleSheet}
          scrollViewRef={scrollViewRef}
          isScrollingEnabled={isScrollingEnabled}
          isSelectedMarker={isSelectedMarker}
        />
      </View>
    );
  }
});

export default MainView;
