import MapView, {Marker} from 'react-native-maps';
import {initialMapState, size28, WINDOW_HEIGHT} from '../../consts/consts';
import React from 'react';
import {StyleSheet} from 'react-native';
import IconFontAwesome5 from 'react-native-vector-icons/FontAwesome5';

const Map = ({toggleSheet, mapRef, mapStore}) => {
  return (
    <MapView
      showPointsOfInterest={false}
      toolbarEnabled={false}
      onPress={() => {
        toggleSheet(false);
      }}
      style={styles.map}
      initialRegion={initialMapState}
      ref={mapRef}>
      {mapStore.markersList.map(marker => (
        <Marker
          key={marker.id}
          tracksViewChanges={false}
          onPress={() => {
            mapStore.onSelectMarker(marker, mapRef);
            toggleSheet(true);
          }}
          coordinate={{
            latitude: marker.latitude,
            longitude: marker.longitude,
          }}>
          <IconFontAwesome5
            name={'lemon'}
            color={marker.color}
            size={size28}
            solid
          />
        </Marker>
      ))}
    </MapView>
  );
};

const styles = StyleSheet.create({
  map: {
    height: WINDOW_HEIGHT,
  },
});

export default Map;
