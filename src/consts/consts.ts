import {Dimensions} from 'react-native';

export const WINDOW_WIDTH = Dimensions.get('window').width;
export const WINDOW_HEIGHT = Dimensions.get('window').height;

export const size6 = WINDOW_WIDTH / 68;
export const size10 = WINDOW_WIDTH / 42;
export const size12 = WINDOW_WIDTH / 34;
export const size14 = WINDOW_WIDTH / 30;
export const size16 = WINDOW_WIDTH / 26;
export const size18 = WINDOW_WIDTH / 23;
export const size20 = WINDOW_WIDTH / 21;
export const size22 = WINDOW_WIDTH / 19.6;
export const size24 = WINDOW_WIDTH / 18;
export const size28 = WINDOW_WIDTH / 15;
export const size32 = WINDOW_WIDTH / 13.5;
export const size34 = WINDOW_WIDTH / 13;
export const size36 = WINDOW_WIDTH / 12;
export const size44 = WINDOW_WIDTH / 10;

export const initialMapState = {
  latitude: 56.837715,
  longitude: 60.603721,
  latitudeDelta: 0.12,
  longitudeDelta: 0.12,
};
