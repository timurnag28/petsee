import {ScrollView} from 'react-native-gesture-handler';
import {StyleSheet, Text} from 'react-native';
import {size12, size18, size24, size44} from '../../consts/consts';
import React from 'react';

const BottomSheetContent = ({
  scrollViewRef,
  isScrollingEnabled,
  isSelectedMarker,
}) => {
  return (
    <ScrollView ref={scrollViewRef} enabled={isScrollingEnabled}>
      <Text style={styles.header}>{isSelectedMarker?.title}</Text>
      <Text
        style={styles.text}>{`latitude: ${isSelectedMarker?.latitude}`}</Text>
      <Text
        style={styles.text}>{`longitude: ${isSelectedMarker?.longitude}`}</Text>
      <Text style={[styles.text, {paddingBottom: size44}]}>
        {isSelectedMarker?.content}
      </Text>
    </ScrollView>
  );
};

const styles = StyleSheet.create({
  header: {
    fontSize: size24,
    color: '#000',
    fontWeight: 'bold',
  },
  text: {
    fontSize: size18,
    color: '#000',
    paddingTop: size12,
  },
});

export default BottomSheetContent;
